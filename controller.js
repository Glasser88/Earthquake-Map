(function() {
  var app = angular.module('earthquakeTracker')


  app.controller("MonthController", function($scope, uiGmapGoogleMapApi, InfoService) {
      // Do stuff with $scope.
      $scope.map = {
        center: {
          latitude: 40.322087,
          longitude: -99.757181
          },
          zoom: 3
          };

          InfoService.getquake().success(function (quakeData) {

            $scope.getQuakeData = quakeData;
            var arrayOfCircles = []

              quakeData.features.forEach(function(el) {
              var windowOptions = {}
              windowOptions['visible'] = false;
              windowOptions['onClick'] = function() {
                     $scope.windowOptions.visible = !$scope.windowOptions.visible;
                 };
              windowOptions['closeClick'] = function() {
                     $scope.windowOptions.visible = false;
                 };
              windowOptions['title'] = "come on";
              var circle = {}
              circle['id'] = el.id;
              circle['center'] = {
                latitude: el.geometry.coordinates[1],
                longitude: el.geometry.coordinates[0]
              };
              circle['geodesic'] = true;
              circle['clickable'] = true;
              circle['date'] = el.properties.time;
              circle['radius'] = el.properties.mag * 10000;
              circle['stroke'] = {
                  color: 'red',
                  weight: 1,
                  opacity: 0.3
              };
              circle['fill'] = {
                  color: 'red',
                  opacity: 0.3
              }
              arrayOfCircles.push(circle);
              //++counter
            });
            $scope.circles = arrayOfCircles;
            console.log(arrayOfCircles);
            console.log(quakeData);

      })

      uiGmapGoogleMapApi.then(function(maps) {


        });

      });

      app.controller("WeekController", function($scope, uiGmapGoogleMapApi, InfoService) {
          // Do stuff with $scope.
          $scope.map = {
            center: {
              latitude: 40.322087,
              longitude: -99.757181
              },
              zoom: 3
              };

              InfoService.getweek().success(function (weekData) {

                $scope.getWeekData = weekData;
                var arrayOfCircles = []

                  weekData.features.forEach(function(el) {
                  var windowOptions = {}
                  windowOptions['visible'] = false;
                  windowOptions['onClick'] = function() {
                         $scope.windowOptions.visible = !$scope.windowOptions.visible;
                     };
                  windowOptions['closeClick'] = function() {
                         $scope.windowOptions.visible = false;
                     };
                  windowOptions['title'] = "come on";
                  var circle = {}
                  circle['id'] = el.id;
                  circle['center'] = {
                    latitude: el.geometry.coordinates[1],
                    longitude: el.geometry.coordinates[0]
                  };
                  circle['geodesic'] = true;
                  circle['clickable'] = true;
                  circle['date'] = el.properties.time;
                  circle['radius'] = el.properties.mag * 10000;
                  circle['stroke'] = {
                      color: 'red',
                      weight: 1,
                      opacity: 0.3
                  };
                  circle['fill'] = {
                      color: 'red',
                      opacity: 0.3
                  }
                  arrayOfCircles.push(circle);
                  //++counter
                });
                $scope.circles = arrayOfCircles;
                console.log(arrayOfCircles);
                console.log(weekData);

          })

          uiGmapGoogleMapApi.then(function(maps) {


            });

          });

          app.controller("DayController", function($scope, uiGmapGoogleMapApi, InfoService) {
              // Do stuff with $scope.
              $scope.map = {
                center: {
                  latitude: 40.322087,
                  longitude: -99.757181
                  },
                  zoom: 3
                  };

                  InfoService.getday().success(function (dayData) {

                    $scope.getDayData = dayData;
                    var arrayOfCircles = []

                      dayData.features.forEach(function(el) {
                      var windowOptions = {}
                      windowOptions['visible'] = false;
                      windowOptions['onClick'] = function() {
                             $scope.windowOptions.visible = !$scope.windowOptions.visible;
                         };
                      windowOptions['closeClick'] = function() {
                             $scope.windowOptions.visible = false;
                         };
                      windowOptions['title'] = "come on";
                      var circle = {}
                      circle['id'] = el.id;
                      circle['center'] = {
                        latitude: el.geometry.coordinates[1],
                        longitude: el.geometry.coordinates[0]
                      };
                      circle['geodesic'] = true;
                      circle['clickable'] = true;
                      circle['date'] = el.properties.time;
                      circle['radius'] = el.properties.mag * 10000;
                      circle['stroke'] = {
                          color: 'red',
                          weight: 1,
                          opacity: 0.3
                      };
                      circle['fill'] = {
                          color: 'red',
                          opacity: 0.3
                      }
                      arrayOfCircles.push(circle);
                      //++counter
                    });
                    $scope.circles = arrayOfCircles;
                    console.log(arrayOfCircles);
                    console.log(dayData);

              })

              uiGmapGoogleMapApi.then(function(maps) {


                });

              });

              app.controller("HourController", function($scope, uiGmapGoogleMapApi, InfoService) {
                  // Do stuff with $scope.
                  $scope.map = {
                    center: {
                      latitude: 40.322087,
                      longitude: -99.757181
                      },
                      zoom: 3
                      };

                      InfoService.getday().success(function (hourData) {

                        $scope.getHourData = hourData;
                        var arrayOfCircles = []

                          hourData.features.forEach(function(el) {
                          var windowOptions = {}
                          windowOptions['visible'] = false;
                          windowOptions['onClick'] = function() {
                                 $scope.windowOptions.visible = !$scope.windowOptions.visible;
                             };
                          windowOptions['closeClick'] = function() {
                                 $scope.windowOptions.visible = false;
                             };
                          windowOptions['title'] = "come on";
                          var circle = {}
                          circle['id'] = el.id;
                          circle['center'] = {
                            latitude: el.geometry.coordinates[1],
                            longitude: el.geometry.coordinates[0]
                          };
                          circle['geodesic'] = true;
                          circle['clickable'] = true;
                          circle['date'] = el.properties.time;
                          circle['radius'] = el.properties.mag * 10000;
                          circle['stroke'] = {
                              color: 'red',
                              weight: 1,
                              opacity: 0.3
                          };
                          circle['fill'] = {
                              color: 'red',
                              opacity: 0.3
                          }
                          arrayOfCircles.push(circle);
                          //++counter
                        });
                        $scope.circles = arrayOfCircles;
                        console.log(arrayOfCircles);
                        console.log(hourData);

                  })

                  uiGmapGoogleMapApi.then(function(maps) {


                    });

                  });


})();
