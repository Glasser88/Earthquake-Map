(function () {
'use strict';

var app = angular.module('earthquakeTracker', [
          'ngRoute',
          'uiGmapgoogle-maps',
          ])
        app.config(function(uiGmapGoogleMapApiProvider, $routeProvider) {
                uiGmapGoogleMapApiProvider.configure({
                key: 'AIzaSyAEx21qDaiRPmhShfVhsUjcz0varjmAvuo',
                v: '3.17',
                libraries: 'weather,geometry,visualization'
            })
            $routeProvider
              .when('/', {
                templateUrl: 'views/week.html',
                controller: 'WeekController'
              })
              .when('/month', {
              templateUrl: 'views/month.html',
              controller: 'MonthController'
              })
              .when('/day', {
              templateUrl: 'views/day.html',
              controller: 'DayController'
              })
              .when('/hour', {
              templateUrl: 'views/hour.html',
              controller: 'HourController'
              })
              .otherwise({
                redirectTo: '/404'
              });

      })
})();
