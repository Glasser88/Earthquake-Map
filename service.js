(function () {
'use strict';
  angular
        .module('earthquakeTracker')
        .factory('InfoService', function($http) {
        var quakeMonthUrl = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_month.geojson';
        var quakeWeekUrl = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_week.geojson';
        var quakeDayUrl = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_day.geojson';
        var quakeHourUrl = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/1.0_hour.geojson';

    var getQuakeData = function () {
    return $http.get(quakeMonthUrl);
    };

    var getWeekData = function () {
    return $http.get(quakeWeekUrl);
    };

    var getDayData = function () {
    return $http.get(quakeDayUrl);
    };

    var getHourData = function () {
    return $http.get(quakeHourUrl);
    };

      return {
      getquake: getQuakeData,
      getweek: getWeekData,
      getday: getDayData,
      gethour: getHourData
      }
    });
})();
